/*
*   Nodo que recibirá velocidades lineales y angulares y realizara un 
*   control desacoplado por simpleza a través de un PID.
*
*/

#include "ros/ros.h"

#include "geometry_msgs/Point.h"
#include "geometry_msgs/Twist.h"
#include "geometry_msgs/Pose.h"
#include "geometry_msgs/Pose.h"
#include "geometry_msgs/Accel.h"
#include "geometry_msgs/Vector3.h"


#include <iostream>
#include <fstream>
#include <math.h>
#include <stdlib.h>
#include <sstream>
